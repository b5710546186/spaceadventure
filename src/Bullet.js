var Bullet = cc.Sprite.extend({
    ctor: function() {
        this._super();
        this.initWithFile( 'res/images/bullet.png' );
    },

    update: function( dt ) {
        this.bulletMove;
    },

    randomPosition: function() {
        var y = 10 + Math.floor( Math.random() * ( screenHeight - 10 ) );
        this.setPosition( new cc.Point( 100, y ) );
    },
    
    bulletMove: function(){
        var x = this.getPositionX();
        x += Bullet.prototype.MOVEX;
        this.setPositionX( x );
    }
});